# Title: Four card feature section

Tech stack: Svelte & Vite

Deployed project: https://saad-shaikh-four-card-feature-section.netlify.app/

## Desktop view:
![Desktop view](design/desktop-design.jpg) 

## Mobile view:
![Mobile view](design/mobile-design.jpg) 
